---
layout: default
title: About Us
---
About us
========

Who we are
----------

The Graduate Physics Student Association is a group for graduate physics
students at the University of Alberta. We share ideas, organize social events,
and try to improve the student life of any grad physics student at University
of Alberta.

Since 2010 the GPSA has been organizing the annual [Symposium for Graduate
Physics Research][05]. The GPSA also conducts the nomination of a faculty
member for the Faculty of Science [Graduate Mentoring Award][10].

Constitution
------------

The GPSA's [constitution][20] outlines exactly how our group functions, and
includes the definitions of each of our executive positions. While it may not be
the most riveting read (sorry!), you may want to try looking through the
Constitution if you're eager to learn more about us!

GPSA Council
------------

### Executives ###

 * Burkhard Ritter
   (President)
 * Zhou Li
   (Vice-President)
 * Sohel Bhuiyan
   (Treasurer)
 * Dunia Blanco
   (Academic Director)
 * Syed Bukhari
   (Social Director)

### GSA Representatives ###

 * Daniel Laycock
   (GSA Representative)
 * Daniel Foster
   (GSA Alternate)

### Focus Area Representatives ###

 * Amr Iberahim
   (Geophysics)
 * Peter Legg
   (Condensed Matter Physics)
 * Burkhard Ritter
   (Condensed Matter Theory)
 * Scott O'Donnell 
   (Astrophysics)
 * Kingsley Emelideme
   (Particle Physics)

The GPSA executives thank the members of the [previous GPSA councils][30] for
their work and commitment.

Election 2012
-------------

On April 4 2012 the GPSA council for 2012/2013 was [elected][40]. Most positions
take office in late August 2012, with the notable exception of the treasurer,
Sohel Bhuiyan, who takes office on April 4 2012. The election included a
by-election for the president. As a result, the new president, Burkhard Ritter,
takes office on April 4 2012.

About this site
---------------

For the technically minded: This site is written in [Markdown][60] and generated
by [Jekyll][70]. The source is hosted at [Bitbucket][80]. The site is currently
maintained by [Burkhard Ritter](mailto:Burkhard Ritter <burkhard@ualberta.ca>).

[05]: {{site.url}}/symposium.html
[10]: {{site.url}}/graduate_mentoring_award.html
[20]: {{site.url}}/files/gpsa_constitution.pdf
[30]: {{site.url}}/council.html
[40]: {{site.url}}/files/results_gpsa_election_2012.pdf
[60]: http://daringfireball.net/projects/markdown/
[70]: https://github.com/mojombo/jekyll
[80]: https://bitbucket.org/meznom/gpsasite
