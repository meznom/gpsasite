---
layout: default
title: Graduate Mentoring Award
---

Graduate Mentoring Award
========================

The [Faculty of Science][10] has created a [Graduate Mentoring Award][20] in
order to recognize the Faculty members who are extraordinary in mentoring the
next generation of scientists. If you believe that your supervisor or any
professor in the [Department of Physics][30] fulfills the requirements to be an
outstanding mentor, please fill the application form ([pdf][40], [doc][45]) and
submit a support letter. 

To consider a professor for the award a minimum 3 and maximum 5 letters of
support from present or past graduate students are required. The letters should
address how the interaction with the professor made a significant difference to
your career. 

The deadline to send the support letter and application is January 16, 2012.
Please feel free to email the GPSA's academic coordinator [Dunia
Blanco](mailto:Dunia Blanco <blancoac@ualberta.ca>) if you have any further
question. 

The Guide for Reviewers ([pdf][50], [odt][60]) provides details about the GPSA
review committee and the nomination process.

[10]: http://www.science.ualberta.ca/
[20]: {{ site.url }}/files/faculty_of_science_graduate_mentoring_award.pdf
[30]: http://www.physics.ualberta.ca/
[40]: {{ site.url }}/files/graduate_mentoring_award_application.pdf
[45]: {{ site.url }}/files/graduate_mentoring_award_application.doc
[50]: {{ site.url }}/files/graduate_mentoring_award_guide_for_reviewers.pdf
[60]: {{ site.url }}/files/graduate_mentoring_award_guide_for_reviewers.odt
