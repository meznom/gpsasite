---
layout: default
title: Symposium
---
Symposium for Graduate Physics Research
=======================================

About
-----

The GPSA organizes the annual Symposium for Graduate Physics Research at the
University of Alberta. Typically, the one-day symposium includes a poster
session and talks from graduate students at the department, together with a talk
by a renowned keynote speaker. The next symposium will take place in September
2012.

2011
----

The second annual Symposium for Graduate Physics Research took place on
September 9, 2011. The keynote speaker was [Dr. David Griffiths][20], author of
Introduction to Electrodynamics, Introduction to Quantum Mechanics, and
Introduction to Elementary Particles. 

2010
----

The first annual Symposium for Graduate Physics Research, held on September 17,
2010, prominently featured a public outreach talk by [Dr. Lawrence M.
Krauss][30]. Dr.  Krauss is a foundation professor at the School of Earth and
Space Exploration, as well as a professor in the physics department and director
of the Origins Initiative at Arizona State University. He is probably most
famous for his book titled "The Physics of Star Trek". The [original symposium
website][40] has all the details.

[20]: http://academic.reed.edu/physics/faculty/griffiths.html
[30]: http://krauss.faculty.asu.edu/
[40]: {{site.url}}/symposium2010/index.html
