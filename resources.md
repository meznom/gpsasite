---
layout: default
title: Resources
---

Resources
=========

Best Paper Award
----------------

The Department of Physics introduced the "Best Paper Award", with the goal to
recognize the graduate student with the most significant contribution to a
publication in a refereed journal. The award over $500 is given out annually.
The details are outlined in a [separate document][05].
Applications should be submitted to Sarah Derr and the winner will be announced
at the GPSA's annual symposium for graduate physics research in September. For
2012 the application deadline is May 1.

Graduate Mentoring Award
------------------------

The GPSA administers the nomination process for the Faculty of Science's
Graduate Mentoring Award at the Department of Physics. The details, including
application forms and deadlines, are on a [separate page][10].

Thesis Template
---------------

Malcolm Roberts from the math department created a [latex thesis template][20]
that conforms with the University of Alberta thesis requirements. The repository
contains a latex poster template and a couple of other useful templates, too.

[05]: {{site.url}}/files/best_paper_award.pdf
[10]: {{site.url}}/graduate_mentoring_award.html
[20]: http://code.google.com/p/ualberta-math-stat-templates/
