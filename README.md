GPSA website
============

The GPSA is the Graduate Physics Student Association at the University of
Alberta. This is the 'source' for the [GPSA website][10]. The site is now a
[public git repository][20] at [Bitbucket][25].

The website is written mostly in [Markdown][30] with a couple of html templates
and css files thrown in. The site is then generated from these source files with
[Jekyll][40], a static website generator that is used for [Github's pages
feature][45]. The site uses some parts from [Jekyll-Bootstrap][47].
Jekyll-Bootstrap is also an excellent resource to learn about and get started with
Jekyll. 

Contributing to the site requires:

 * [Git][50] and minimal knowledge about how to use it.
 * [Ruby][60] as Jekyll is written in Ruby. It's preinstalled on Mac OS X, on
   Ubuntu installation is trivial.
 * [Jekyll][40]. Installation and usage is outlined on its website. We are
   currently using rdiscount as the markdown renderer. Typically it can be
   installed with:  
   `$ sudo gem install rdiscount`
 * Probably an account on [Bitbucket][25], at least as long as we keep
   'development' there.

A typical contribution might look like this:

 * Clone the repository  
   `$ git clone https://bitbucket.org/meznom/gpsasite.git`  
   or if there's already a local copy of the repository, pull in the latest changes  
   `$ git pull`
 * Do something. For example, write a news item by creating a new file in `_posts`.
 * View the updated site locally by running `jekyll --server --auto` and pointing
   a browser at <http://0.0.0.0:4000/~physgpsa/>.
 * Commit the changes.  
   `$ git commit -a`
 * Push changes to the public repository (this assumes write access).  
   `$ git push`
 * Deploy the updated website by uploading the generated site (in `_site`) to
   the gpsa account on the university's file server (gpu.srv.ualberta.ca).
   Conveniently, there's a shell script that does this.  
   `$ ./_tasks/deploy.sh`

Without write access to the public repository the process would likely be
altered to first fork the repository and later on create a pull request.

When using [Vim][70], to have the ".md" file extension recognized as markdown,
add the following line to the ".vimrc" file: 

    autocmd BufRead,BufNewFile *.md set filetype=markdown

Burkhard Ritter (<burkhard@seite9.de>), April 2012.


TODO
----

 * pictures of executives
     * list members of previous executives somewhere
 * new design
     * probably based on the design of the department's site, I like the
       colors, fonts, etc
     * lighter. It should reflect and accomodate that we don't have a lot of
       content.
 * new logo
     * either only text based
     * or some funky physics symbol (Kevin uses hbar for his site) or,
       possibly, some Feynman diagrams
     * Logan: competition (grads mailing list)
 * link to department and university should be placed prominently (on every
   page)
 * link to BearsDen and Facebook (possibly in the footer)
 * more up-to-date pictures for gallery
 * how well can we do a picture gallery with Jekyll?
 * maybe get rid of links page
 * list of graduate students?
     * better list than the list on department's site
     * pictures
     * sortable by alphabet, supervisor and focus area
     * would be better if the list on the department site would be improved
 * link to tutoring list
 * push news to bearsden and facebook
 * do we need twitter?
 * links: maybe beartracks
 * what is the audience of our site? grad students? or more external people
 * make old versions of constitution accessible


Ideas for a gallery plugin
--------------------------

There are already a couple of rough gallery plugins for jekyll. 

 * <https://github.com/mojombo/jekyll/pull/303>
 * <https://github.com/tsmango/jekyll_flickr_set_tag/blob/master/_plugins/flickr_set.rb>
 * <http://baldowl.github.com/2011/04/13/rough-gallery-plugin-for-jekyll.html>

At the moment the last approach looks most appealing to me. The Liquid block
approach (first in the list above) might work as well. For a gallery 'foo' on a
given page the plugin should first look in the current directory for the
subdirectory 'foo' (possibly also in all parent directories) and in a global
gallery-directory. If the specified directory contains subdirectories then those
are made available as albums in the data structure (more than one level of
nesting is likely not necessary). Pictures are made available in alphabetical
order. One special file is the cover picture for the album. Thumbnails are in
the special subdirectory thumbnails. If the plugin could generate those
thumbnails that would be awesome. My idea is that a given page should be able to
contain multiple albums.

Miscellaneous links:

 * gallery styling with css <http://www.css-zibaldone.com/articles/gallery/styling.html>
 * lightbox <http://www.lokeshdhakar.com/projects/lightbox2/>
 * jquery based image galleries <http://www.webdesignerdepot.com/2011/08/25-jquery-image-galleries-and-slideshow-plugins/>


[10]: http://www.ualberta.ca/~physgpsa/
[20]: https://bitbucket.org/meznom/gpsasite
[25]: https://bitbucket.org/
[30]: http://daringfireball.net/projects/markdown/
[40]: https://github.com/mojombo/jekyll
[45]: http://pages.github.com/
[47]: http://jekyllbootstrap.com/
[50]: http://git-scm.com/
[60]: http://www.ruby-lang.org/en/
[70]: http://www.vim.org/
