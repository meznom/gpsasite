---
layout: default
title: UofA Graduate Physics Student Association
---

Welcome to the Graduate Physics Student Assocication
====================================================

Who we are
----------

The Graduate Physics Student Association is a group for graduate physics
students at the University of Alberta. We share ideas, organize social events,
and try to improve the student life of any grad physics student at University
of Alberta.

News
----

{% for post in site.posts limit:10 %}
{% include post.html %}
{% endfor %}

News archive
------------

Older news posts are in the [news archive][10].

[10]: {{site.url}}/archive.html
