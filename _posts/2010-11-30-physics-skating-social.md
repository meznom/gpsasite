---
title: Physics Skating Social
date: Tue, 30 Nov 2010 10:00:00 EST
layout: post
---
It's time to dust off your skates! The GPSA is hosting this year's Department
Skating Social next Thursday, December 9, from 10-11am at the Clare Drake
arena.
