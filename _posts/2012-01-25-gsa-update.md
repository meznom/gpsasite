---
title: GSA Update
date: 2012-01-25
layout: post
---
Daniel has two updates from the latest GSA council meeting:

> 1. [Nominations](http://www.gsa.ualberta.ca/index.php/governance/content/category/elections/)
> are now open for the positions of President, Vice Presidents, and ten
> Councillors-at-Large for the 2012 GSA Council.  Nominations are due by February
> 16, and the election will begin on March 7.
> 
> 2. The annual [GSA Awards Night](http://www.gsa.ualberta.ca/index.php/funding/content/category/awards/) will be held on March 14.  The deadline
> to nominate yourself, or someone else, for an award is January 31.
> 
> As usual, all the [exciting details from the
> meeting](http://www.gsa.ualberta.ca/index.php/governance/content/category/council_minutes_and_agendas/)
> can be found on the GSA's website.
