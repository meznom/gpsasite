---
title: GSA Update
date: 2011-11-01
layout: post
---
Again, our GSA representative Daniel has a couple of updates from the latest
GSA council meeting:

> 1. The GSA is hosting a screening of [The PhD
> Movie](http://www.phdcomics.com/movie/aboutmovie.html) on November 17 at 7pm
> in CCIS 1-440. Entrance is free but donations to the Campus Food Bank will be
> accepted.
> 
> 2. The design of the new Physical Activity and Wellness (PAW) centre, to be
> built next to the Butterdome, is nearly complete. The GSA is still in
> negotiations with the other parties on a usage agreement.
> 
> As usual, all the [exciting details from the
> meeting](http://www.gsa.ualberta.ca/index.php/governance/content/category/council_minutes_and_agendas/)
> can be found on the GSA's website.
