---
title: New executive takes office
date: 2011-09-01
layout: post
---
As of today the new elected [GPSA executive]({{site.url}}/about_us.html) will
take office. A big thank you to last year's executive for doing a wonderful job!
