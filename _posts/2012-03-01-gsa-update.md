---
title: GSA Update
date: 2012-03-01
layout: post
---
Here's the latest update from the GSA council meetings:

> 1. A [general
>    election](http://www.gsa.ualberta.ca/index.php/governance/content/category/elections/)
>    for the 2012 GSA Council will be held from March 7-9. 
> 
> 2. The GSA received an update from the University on their proposal to switch
>    to a bi-monthly pay cycle.  The motivation for the change is to eliminate
>    the inefficiencies of the current system which requires many off-cycle
>    cheques to be prepared.
> 
>    What this means for graduate students is the following:
>     * Under the current pay cycle, all income earned during a given month is
>       paid out on the second last business day of the month.
>     * As of July 2012, the plan is that pay will be distributed twice each
>       month.
>     * Employment income will now be held back roughly 10 days from the end of
>       the pay period.  Pay for the first 15 days of the month will be
>       distributed on the 25th, and for the rest of the month on the 10th of the
>       following month.
>     * July will be a transitional month.  Instead of receiving your full
>       employment income on the second last business day (July 30), you will
>       receive 1/2 on July 25 and1/2 on August 10.
>     * To help students bridge this transition, the University has agreed to
>       provide an interest-free loan of 70% (and possibly more) of the held back
>       amount (ie 70% of the August 10 pay) on July 25th for any student who
>       requests it.  The loan would be repaid by equal deductions from your pay
>       for the duration of your employment period.  Since the loan is interest
>       free, there is no reason not to apply for it if you need the money.
>     * Note that only employment income will be held back.  Any monthly income
>       derived from scholarships will be distributed in full on July 25.  If you
>       currently make $1000/month from scholarships, you will received $1000 on
>       July 25 and then $500 every following 10th and 25th.
>     * The University is planning on holding a town hall meeting to explain
>       these changes.  Keep your eyes peeled for an email from them.
> 
> As usual, all the [exciting details from the
> meeting](http://www.gsa.ualberta.ca/index.php/governance/content/category/council_minutes_and_agendas/)
> can be found on the GSA's website.
