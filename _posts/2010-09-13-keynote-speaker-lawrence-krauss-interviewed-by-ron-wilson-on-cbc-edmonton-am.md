---
title: Keynote speaker, Lawrence Krauss, interviewed by Ron Wilson on CBC Edmonton AM
date: Mon, 13 Sept 2010 08:00:00 EST
layout: post
---
[Listen](http://podcast.cbc.ca/mp3/edmontonam_20100913_38076.mp3) to Lawrence
Krauss interviewed by Ron Wilson on CBC Edmonton AM, 4 days before his visit to
UA.
