---
title: GPSA members approve new constitution
date: 2011-10-05
layout: post
---
At today's general assembly, the majority of GPSA members have voted in favour
of a few amendments proposed for the GPSA constitution. The [new constitution](
{{site.url}}/files/gpsa_constitution.pdf) introduces two major changes:

1. Defining a quorum "minimum number of attendees" for any instance where a vote
   is necessary.
2. Defining a method for voting by secret ballot either at a meeting or
   electronically (so attending future meetings will not necessarily be required
   in order for members to vote).
