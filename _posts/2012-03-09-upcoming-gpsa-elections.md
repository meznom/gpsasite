---
title: Upcoming GPSA elections
date: 2012-03-09
layout: post
---
The elections for the GPSA executives for 2011/2012 will be coming very soon.
We encourage you to consider taking over any of the
[positions]({{site.url}}/about_us.html) and getting involved with the GPSA. We
will announce the date and place for the elections shortly.
