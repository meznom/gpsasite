---
title: GSA Update
date: 2011-10-05
layout: post
---
Our GSA representative Daniel Laycock has news from the latest GSA council meeting.

>  1. The University has proposed a 1.45% increase in tuition for the 2012-13
>  year, which is based on changes to the CPI (consumer price index).  For
>  comparison, our collective agreement with the University calls for an
>  increase in remuneration of 2%.
>  
>  2. A new professional development award has been announced by the
>  University, the [Green and Gold Student Leadership and Professional
>  Development Grant Adjudication] (http://www.greenandgoldgrant.ualberta.ca/).
>  
>  3. The Academic Policy and Process Review Task Force (APPRTF) is currently
>  in the process of reviewing and updating University policies. Among the
>  notable changes, the regulation permitting the tethering of horses outside
>  of academic buildings has been revoked.
>  
>  As usual, all the [details from the
>  meeting](http://www.gsa.ualberta.ca/index.php/governance/content/category/council_minutes_and_agendas/)
>  can be found on the GSA's website.
