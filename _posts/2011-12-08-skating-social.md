---
title: Department of Physics Skating Social
data: 2011-12-08
layout: post
---
Come out to this year's Department of Physics Skating social! It's at the Clare
Drake Arena on December 21 starting 11am. There will be opportunity both to
skate and play hockey. Coffee, hot chocolate and snacks will be available to
sustain you on the ice.

The event is co-organized by the GPSA and we are currently looking for
volunteers. Please contact [Syed](mailto:smanzoor@ualberta.ca) if you want to
help or bring food.
