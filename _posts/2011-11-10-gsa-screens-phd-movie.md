---
title: GSA screens PHD movie
date: 2011-11-10
layout: post
---
The Graduate Students' Association screens [The PhD Movie](
http://www.phdcomics.com/movie/index.php) on November 17, 2011 in CCIS 1-440 at
7:00pm. It's the first movie from the popular [PhD Comics](
http://www.phdcomics.com). Admission is free, but donations to the Campus Food
Bank will be accepted.
