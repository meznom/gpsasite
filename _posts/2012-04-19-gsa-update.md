---
title: GSA Update
date: 2012-04-19
layout: post
---
Our new GSA representative Colin has the latest news from the GSA council:

The April GSA meeting happened this past Monday, so I thought I'd give you all a
brief rundown of the highlights:

Like it or not, the University is going ahead with the change in pay cycle.
Starting on July 1st, we'll start getting paid every two weeks (or so).  Pay
dates are on the 10th and 25th each month, unless that day is one when the banks
are not open, in which case we are paid on the last banking day before the usual
pay day. In case this will screw up various bills and other living expenses you
have, all grad students are eligible for an interest-free loan equal to 70% of
your usual monthly pay. If this still doesn't work for you, we've gotten all
sorts of assurances from Human Resources that they'll do what they can to help
you out, so get in touch with them as soon as possible! There's also a fairly
useful [website](http://www.se2.ualberta.ca) the school has set up for the
changeover.

Our health & dental plan fees are going up next year, to just shy of $400.  The
actual premiums are more than this, with the difference being paid out of
surplus from previous years by the GSA.
