---
title: End-of-term Physics Party!
date: Tue, 30 Nov 2010 10:00:00 EST
layout: post
---
The Undergraduate Physics Society (UPS) is hosting its end-of-term physics
party at Dewey's on Wednesday, December 1, 2010. Get tickets through the GPSA
in CEB 3-14.
