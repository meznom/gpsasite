---
title: Undergraduate End of Term Party
date: 2011-11-21
layout: post
---
The Undergraduate Physics Society's end of term party will take place on
November 30th at [Dewey's][1]. The theme this year will be a Meme party and
tickets are $3 ahead of time or $5 at the door. Graduate students can buy
tickets from their focus area representatives.

[1]: http://www.su.ualberta.ca/businesses/deweys/
