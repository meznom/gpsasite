---
title: GPSA elections
date: 2012-03-20
layout: post
---
The GPSA elections will be held in room L1-047 on Friday March 30th at 4:00pm.
An online ballot will open the same date and time and remain open for one week.

Nominations for any [GPSA position]({{site.url}}/about_us.html) should be send
to [Daniel Laycock](mailto: Daniel Laycock <tlaycock@ualberta.ca>). The deadline
for nomination is 12:00am on March 27th.
