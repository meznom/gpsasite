---
title: GPSA Symposium for Graduate Physics Research
date: 2011-07-21
layout: post
---
The Graduate Physics Students Association (GPSA) of the University of Alberta is
hosting its second annual Symposium for Graduate Physics Research this September
9th, 2011. This year our keynote speaker will be Dr. David Griffiths, author of
Introduction to Electrodynamics, Introduction to Quantum Mechanics, and
Introduction to Elementary Particles. Dr. Griffiths will be giving a special
colloquium, preceded by a short series of graduate student talks. We will also
have a poster session for undergraduate and graduate students in the Department
of Physics.
