---
title: Faculty of Science Graduate Mentoring Award
date: 2011-10-26
layout: post
---
The faculty of science has created a [Graduate Mentoring
Award]({{site.url}}/files/faculty_of_science_graduate_mentoring_award.pdf) in
order to recognize the Faculty members who are extraordinary in mentoring the
next generation of scientist. To consider a professor for the award a minimum 3
and maximum 5 letters of support from present or past graduate students are
required. Graduate students who want to nominate a professor should send the
support letter and the filled out [application
form]({{site.url}}/files/graduate_mentoring_award_application.pdf) to the GPSA
mail box by January 16, 2012.
