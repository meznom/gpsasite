---
title: GPSA election results
date: 2012-04-04
layout: post
---
This year's election was concluded last night with over 40% of the GPSA members
electing the new council. Congratulations to the new GPSA council members: 

 * Burkhard Ritter (President)
 * Amir Ibrahim (Vice President)
 * Sohel Bhuiyan (Treasurer)
 * Daniel Laycock (Academic Director)
 * Khaled Elshamouty (Social Director)
 * Colin More (GSA Representative)
 * Nasser Kazemi Nojadeh (Geophysics Representative)
 * Adeel Akram (Particle Physics Representative)
 * Abigail Stevens (Astrophysics Representative)
 * Ayesheshim Ayesheshim (Experimental Condensed Matter Physics Representative)
 * Tokunbo Omiyinka (Theoretical Condensed Matter Physics Representative)

Most positions will take office in late August, with the notable exception of
the treasurer and the president who both take office today. The new president
takes over early, because the outgoing president, Khaled Barakat, is leaving the
department. A huge thank you to Khaled and also Logan Sibley, the outgoing
treasurer. Please have a look at the [detailed elections
results]({{site.url}}/files/results_gpsa_election_2012.pdf).
