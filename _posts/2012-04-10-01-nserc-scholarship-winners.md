---
title: NSERC Scholarship Winners
date: 2012-04-10
layout: post
---
Congratulations to our members who won a NSERC MSc scholarship this year!

 * Andrei Catuneanu
 * Callum Doolin
 * Megan Engel
 * Bradley Hauer
 * Joel Hutchinson
 * Stephen Portillo
