---
title: Best Paper Award
date: 2012-04-10
layout: post
---
The Department of Physics announced a new award, the "Best Paper Award". The
award over $500 is given out annually to the graduate student with the most
significant contribution to a publication in a refereed journal. Please read up
on the [details of the award]({{site.url}}/files/best_paper_award.pdf). The
deadline is May 1 and applications should be handed to Sarah Derr. The winner
will be announced at the GPSA's annual symposium for graduate physics research
in September.
