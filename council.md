---
layout: default
title: GPSA Council
---
GPSA Council
============

2011-2012
---------

 * Khaled Barakat
   (President, September 2011 - March 2012)
 * Burkhard Ritter
   (President, April 2012 - August 2012)
 * Zhou Li
   (Vice-President)
 * Logan Sibley
   (Treasurer)
 * Dunia Blanco
   (Academic Director)
 * Syed Bukhari
   (Social Director)
 * Daniel Laycock
   (GSA Representative)
 * Daniel Foster
   (GSA Alternate)
 * Amr Iberahim
   (Geophysics Representative)
 * Peter Legg
   (Condensed Matter Physics Representative)
 * Burkhard Ritter
   (Condensed Matter Theory Representative)
 * Scott O'Donnell 
   (Astrophysics Representative)
 * Kingsley Emelideme
   (Particle Physics Representative)

2010-2011
---------

 * Logan Sibley
   (President)
 * Rob McLeod
   (Vice-President)
 * Dan Foster
   (Treasurer)
 * Tyrone Woods
   (Academic Director)
 * Mizan Chowdhury
   (Social Director)
 * Karol Rohraff
   (GSA Representative)
 * Dan Foster
   (GSA Alternate)
 * Nadia Kreimer
   (Geophysics Representative)
 * Shawn Compton
   (Condensed Matter Physics Representative)
 * Travis Craddock
   (Condensed Matter Theory Representative)
 * Azizul Hoque
   (Astrophysics Representative)
 * Matthew Dowling
   (Particle Physics Representative)

2009-2010
---------

 * Laura Mazzino
   (President)
 * Logan Sibley
   (Vice-President)
 * Jen Moroz
   (Social Director)
 * David Miles
   (GSA Representative)
 * Josh Mutus
   (GSA Alternate)
 * Fatimah Abuduwufu (replaced by Nadia Kreimer at half-term)
   (Geophysics Representative)
 * Tyler Cocker
   (Condensed Matter Physics Representative)
 * Travis Craddock
   (Condensed Matter Theory Representative)
 * Tyrone Woods
   (Astrophysics Representative)
 * Kevin Olsen
   (Particle Physics Representative)
 * Ben Burke
   (Medical Physics Representative)

2008-2009
---------

 * Steve Olson
   (President)
 * Laura Mazzino
   (Vice-President)
